import dataclasses
from .game import GameState
from .player import Player

ALICE = Player(name='Alice')
BOB = Player(name='Bob')
CHARLIE = Player(name='Charlie')

TWO_PLAYER_GAME = GameState(players=[ALICE, BOB])
THREE_PLAYER_GAME = GameState(players=[ALICE, BOB, CHARLIE])

def test_100_1a():
    assert TWO_PLAYER_GAME.two_player == True
    assert THREE_PLAYER_GAME.two_player == False
    lost_player_game = dataclasses.replace(THREE_PLAYER_GAME, players=[BOB, CHARLIE])
    assert lost_player_game.two_player == False

def test_100_1b():
    assert TWO_PLAYER_GAME.multiplayer == False
    assert THREE_PLAYER_GAME.multiplayer == True
    lost_player_game = dataclasses.replace(THREE_PLAYER_GAME, players=[BOB, CHARLIE])
    assert lost_player_game.multiplayer == True
