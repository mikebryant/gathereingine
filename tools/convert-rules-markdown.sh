#!/bin/sh

set -eux

curl https://media.wizards.com/2020/downloads/MagicCompRules%2020200807.txt |
  dos2unix |
  sed -E '/^Credits/,/^Glossary/s/^([0-9.]+)/[ ] \1/g' |
  cat > rules.md
