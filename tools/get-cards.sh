#!/bin/sh

set -eux

curl https://mtgjson.com/api/v5/AtomicCards.json.bz2 | bunzip2 > cards.json
