import copy
import dataclasses
from typing import List

from .player import Player

@dataclasses.dataclass(frozen=True)
class GameState:
    players: List[Player]
    _initial_players: List[Player] = None

    def __post_init__(self):
        if self._initial_players is None:
            super().__setattr__('_initial_players', copy.copy(self.players))

    @property
    def two_player(self):
        return len(self._initial_players) == 2

    @property
    def multiplayer(self):
        return not self.two_player
